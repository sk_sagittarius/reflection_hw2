﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reflection_hw2
{
    public class Book
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public double Price { get; set; }

        public Book()
        {
            Id = Guid.NewGuid();
        }

        public void Show()
        {
            Console.WriteLine($"Name: {Name}, Author: {Author}, Price: {Price}");
        }

    }
}
