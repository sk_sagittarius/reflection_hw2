﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Reflection_hw2
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book
            {
                Name = "The Jungle Book",
                Author = "Joseph Rudyard Kipling",
                Price = 1000
            };

            book.Show();
            Console.ReadLine();


            //Type type = typeof();
            Type type = book.GetType();

            try
            {

                foreach (var element in type.GetMembers())
                {
                    Console.WriteLine(element);
                    Console.WriteLine("-----------");

                    if (type.IsClass && element.MemberType == MemberTypes.Method && element.Name.Contains("Show"))
                    {
                        Console.WriteLine((element as MethodInfo).Invoke(book, new object[] { }));
                    }

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
